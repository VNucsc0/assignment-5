#include <stdio.h>

int cal_attendance(int price){
    const int att = 120;
    return att - ((price-15) /5*20);
}
int cal_revenue(int price){
    return price * cal_attendance(price);
    
}
int cal_expend(int price){
    const int x = 500;
    return x + 3 * cal_attendance(price); 
}
int cal_profit(int price){
    return cal_revenue(price)-cal_expend(price);
}
void display(){
    int j=5,profit;
    for (j=5;j<=50;j+=5){
        profit= cal_profit(j);
        printf("Rs.%d \t Rs.%d\n",j,profit);
        
    }
}
int main()
{
    printf("The relationship between ticket price and profit displays on the table\n");
    printf("Price \t Profit\n");
    display();
    printf("According to the table it can determine which ticket price makes the highest profit");
    
    return 0;
}